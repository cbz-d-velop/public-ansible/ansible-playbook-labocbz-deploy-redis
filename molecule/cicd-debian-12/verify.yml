---
# Don't forget to keep this file updated
# molecule/<scenario>/verify.yml
- name: "Verify:role-prepare-host"
  hosts: "cicd-debian-12:&role-prepare-host"
  gather_facts: true
  tasks:

    - name: "Check if all provided packages are installed"
      register: "package_check"
      loop: "{{ inv__prepare_host__packages_to_install__install_redis }}"
      loop_control:
        loop_var: "package"
      failed_when: "package_check.rc != 0"
      ansible.builtin.command: "dpkg -l {{ package }}"

    - name: "Check if all provided groups are present"
      register: "group_check"
      loop: "{{ inv__prepare_host__users_to_add__install_redis }}"
      loop_control:
        loop_var: "group"
      failed_when: "group_check.rc != 0"
      ansible.builtin.command: "getent group {{ group.group }}"

    - name: "Check if all provided users are present"
      register: "user_check"
      loop: "{{ inv__prepare_host__users_to_add__install_redis }}"
      loop_control:
        loop_var: "user"
      failed_when: "user_check.rc != 0"
      ansible.builtin.command: "id -Gn {{ user.login }}"

- name: "Verify:role-install-redis"
  hosts: "cicd-debian-12:&role-install-redis"
  gather_facts: true
  tasks:

    - name: "Get Redis service current state"
      register: "install_redis__service_status"
      failed_when: "not install_redis__service_status.status.ActiveState == 'active'"
      ansible.builtin.systemd:
        name: "redis-server"

    - name: "Check Redis connectivity"
      ansible.builtin.wait_for:
        host: "{{ inventory_hostname }}"
        port: "{{ inv__install_redis.port }}"
        timeout: 120

    - name: "Set args: Auth / TLS"
      ansible.legacy.set_fact:
        redis_auth: "-a {{ inv__install_redis.requirepass }} --no-auth-warning"
        redis_tls: "--tls"
        redis_mtls: ""

    - name: "Set args: mTLS"
      when: "inv__install_redis.client_auth"
      ansible.legacy.set_fact:
        redis_mtls: "--cert {{ inv__install_redis.ssl_crt }} --key {{ inv__install_redis.ssl_key }}"

    - name: "Verify PING"
      register: "install_redis__result"
      failed_when: "install_redis__result.stdout != 'PONG'"
      changed_when: "install_redis__result.rc != 0"
      ansible.builtin.shell: |
        redis-cli -h {{ inventory_hostname }} -p {{ inv__install_redis.port }} {{ redis_auth }} {{ redis_tls }} {{ redis_mtls }} ping

    - name: "Check Redis Cluster installation with Sentinel"
      when: "inv__install_redis__cluster.enabled | default(false)"
      block:
        - name: "Get Sentinel service current state"
          register: "install_redis__service_status"
          failed_when: "not install_redis__service_status.status.ActiveState == 'active'"
          ansible.builtin.systemd:
            name: "redis-sentinel"

        - name: "Check Sentinel connectivity"
          ansible.builtin.wait_for:
            host: "{{ inventory_hostname }}"
            port: "{{ inv__install_redis__cluster.sentinel_port }}"
            timeout: 120

        - name: "Verify role on MASTER"
          register: "install_redis__result"
          when: "inventory_hostname == inv__install_redis__cluster.initial_mastername"
          failed_when: "install_redis__result.stdout != 'role:master'"
          changed_when: "install_redis__result.rc != 0"
          ansible.builtin.shell: |
            redis-cli -h {{ inventory_hostname }} -p {{ inv__install_redis.port }} {{ redis_auth }} {{ redis_tls }} {{ redis_mtls }} info | grep role:

        - name: "Verify role on MASTER"
          register: "install_redis__result"
          when: "inventory_hostname != inv__install_redis__cluster.initial_mastername"
          failed_when: "install_redis__result.stdout != 'role:slave'"
          changed_when: "install_redis__result.rc != 0"
          ansible.builtin.shell: |
            redis-cli -h {{ inventory_hostname }} -p {{ inv__install_redis.port }} {{ redis_auth }} {{ redis_tls }} {{ redis_mtls }} info | grep role:
